# Для работы с временем
import time
from datetime import datetime
# Для работы с ботами
import openai
import telebot;
from telebot import types

#---Получаем токены из текстового файла---#
fileTokens = open("tokens.txt", "r")
tokens = fileTokens.readlines()

#---Присваиваем токен для телеграм бота---#
token_bot_telegram = tokens[0].replace('bot_telegram_token=','')
token_bot_telegram = token_bot_telegram[0: len(token_bot_telegram)-1]
bot = telebot.TeleBot(token_bot_telegram)

#---Присваиваем токен для ChatGPT---#
api_key = tokens[1].replace('openai_token=','')
openai.api_key = api_key

#------Переменные и DB для работы с ботом------#
startPromt = ''
DB = {}

#------Функция, хранящая и отправляющая сообщения------#
def SendMessages(typeMessage):

    messageList = ["👋 Привет! Я чат-бот для общения и поддержки своего собеседника. Напиши мне и мы поговорим с тобой. Если нужно сбросить диалог или получить контакты для оказания помощи, то воспользуйся кнопками снизу! Жду твоего сообщения. ⏳",
                    'Получить контакты 📞',
                    'Сбросить диалог 🗑️',
                    'Инструкция 📖',
                    "Ожидаем ответ от чат-бота... 🔎",
                    'История очищена 🗑️',
                    'Если Вам нужна помощь, то Вы можете обратиться за ней в Психолого-педагогический центр ПГНИУ 👓',
                    'Контактный номер телефона: +7-342-2-396-776 📞']

    match typeMessage:
        case 'startMessage': return messageList[0]
        case 'contactsButton': return messageList[1]
        case 'clearDialogButton': return messageList[2]
        case 'guideButton': return messageList[3]
        case 'waitAnswer': return messageList[4]
        case 'historyClear': return messageList[5]
        case 'helpInfo1': return messageList[6]
        case 'helpInfo2': return messageList[7]


def LogWrite(message, id):
    """
    Логирование действий пользователя
    """
    directory = 'logs/'
    fileName = id

    current_datetime = str(datetime.now())

    logFile = open(directory+str(fileName)+'.txt', 'a', encoding="utf-8")
    logFile.write(current_datetime + ' ' + message+'\n')
    logFile.close()
    
def generate_prompt(prompt):
    response = openai.Completion.create(
        engine="text-davinci-003",  # Выберите GPT-3.5-turbo
        prompt=prompt,
        temperature=0.1,        # Управление температурой для разнообразия ответов
        max_tokens=500,         # Максимальное количество символов в ответе
        n=1,                    # Количество ответов
        stop=None               # Можете использовать stop для ограничения ответа до определенного фрагмента текста
    )
    return response.choices[0].text.strip()
    
#-----Функция при работе с командой старт-----#
@bot.message_handler(commands=['start'])
def start(message):
    #---Если пользователя нет в Базе Данных---#
    if message.from_user.id not in DB:
        DB[message.from_user.id] = startPromt

    #---Вывод меню и кнопок пользователю---#
    markup = types.ReplyKeyboardMarkup(resize_keyboard=True)
    btn1 = types.KeyboardButton(SendMessages('contactsButton'))
    btn2 = types.KeyboardButton(SendMessages('clearDialogButton'))
    btn3 = types.KeyboardButton(SendMessages('guideButton'))
    markup.add(btn1, btn2, btn3)
    bot.send_message(message.from_user.id, SendMessages('startMessage'), reply_markup=markup)
    LogWrite(message.text, message.from_user.id)

@bot.message_handler(content_types=['text'])
def start_menu(message):
    #---Если пользователя нет в Базе Данных---#
    if message.from_user.id not in DB:
        DB[message.from_user.id] = startPromt

    #---Пользователь получает контакты---#
    if message.text == SendMessages('contactsButton'):
        bot.send_message(message.from_user.id, SendMessages('helpInfo1'), parse_mode='Markdown')
        bot.send_message(message.from_user.id, SendMessages('helpInfo2'), parse_mode='Markdown')
        photo = open('images/photo.jpg', 'rb')
        bot.send_photo(message.from_user.id, photo)
        LogWrite(message.text, message.from_user.id)
    #---Пользователь очищает диалог---#
    elif message.text == SendMessages('clearDialogButton'):
        DB[message.from_user.id] = startPromt
        bot.send_message(message.from_user.id, SendMessages('historyClear'), parse_mode='Markdown')
        LogWrite(message.text, message.from_user.id)
    #---Пользователь хочет получить гайд---#
    elif message.text == SendMessages('guideButton'):
        bot.send_message(message.from_user.id, 'Прочитать инструкцию Вы можете по ' + '[ссылке](https://telegra.ph/Kak-polzovatsya-botom-Psihologicheskaya-pomoshch-12-04)' , parse_mode='Markdown')
        LogWrite(message.text, message.from_user.id)
        pass
    #---Пользователь ввёл сообщение---#
    else:
        bot.send_message(message.from_user.id, SendMessages('waitAnswer'), parse_mode='Markdown')
        user_prompt = message.text
        generated_response = generate_prompt(user_prompt)
        time.sleep(2)
        bot.send_message(message.from_user.id, generated_response, parse_mode='Markdown')
        bot.delete_message(message.from_user.id, message.message_id+1)
        LogWrite(message.text, message.from_user.id)

bot.polling(none_stop=True, interval=0)