<h1 align="center">PyTalkingBot</h1>

<p align="center">
	<img src="/images/tb1.png?cachefix" />
</p>

### Описание:
Данный код представляет собой основу для Телеграм бота, выполненного в рамках проекта "Психологическая помощь",
с интеграцией LLM ChatGPT от OpenAI. 
<p align="center">
	<img src="/images/tb2.png?cachefix" />
</p>

### System requirements: 
- Python 3.11.6
- Python libraries
- - openai==0.28
- - pyTelegramBotAPI==4.16.1
- - telebot==0.0.5

## Как использовать:
```bash
git clone https://gitlab.com/veon_portfolio/pytalkingbot.git
cd pytalkingbot
pip install -r requirements.txt
python3 bot.py 
```


### Info:
- Support: blastyboom2001@gmail.com
<br />
(c) TheVeon
